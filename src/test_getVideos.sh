#!/bin/bash

src/getVideos.py \
  -f data/test_search.txt \
  -k config/yt_client_secrets.json \
  -n 10 \
  -q data/test02_queries.csv \
  -r data/test02_results.csv \
  -z data/test02_transcripts_$(date +"%Y-%m-%dT%H:%M:%S").zip
