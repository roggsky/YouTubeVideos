#!/bin/bash

src/getVideos.py \
  -f config/search.txt \
  -k config/yt_client_secrets.json \
  -q data/queries.csv \
  -r data/results.csv \
  -z data/transcripts_20200625.zip
