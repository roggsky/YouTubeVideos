#!/usr/bin/env python3

import argparse
import googleapiclient.discovery
import googleapiclient.errors
import json
import os
import sys
import time
from youtube_transcript_api import YouTubeTranscriptApi
import zipfile
import zlib

global channel_dict, query_file, query_id, results_file, state_file, video_set, zip_file

channel_dict = dict()
num_results = -1
query_file = None
query_id = -1
quota_exceeded = False
results_file = None
state_file = None
video_set = set()
zip_file = None

def access( var, list, defval ):
    try:
        retval = var
        for idx in list:
            retval = retval[ idx ]
    except KeyError as ke:
        retval = defval
    return retval

def check_quota( msg ):
    if 'quotaExceeded' in msg:
        state_file.close()
        if query_file != None:
            query_file.close()
        if results_file != None:
            results_file.close()
        if zip_file != None:
            zip_file.close()
        exit(1)


def process_video(vid=None):
    global query_file, query_id, results_file, video_set, zip_file

    try:
        transcripts = YouTubeTranscriptApi.list_transcripts(vid)

        transcript = transcripts.find_transcript(['en','en-US','en-CA','en-GB', 'en-IN'])
        data = transcript.fetch()

        content = "\n".join(map( lambda e: e['text'], data ))
        if zip_file == None:
            with open(vid+".txt","w") as fout:
                print(content,fout)
        else:
            zip_file.writestr(vid+".txt",content,compress_type=zipfile.ZIP_DEFLATED)

    except Exception as exc:
        msg = str(exc.args)
        print( "\nERROR - "+vid+" Exception raised during video trancscription access:", file=sys.stderr)
        print(msg, file=sys.stderr)
        check_quota( msg )


def video_search( api_key=None, search_phrase=None ):
    global channel_dict, query_file, query_id, results_file, video_set, zip_file

    query_id = query_id + 1

    api_service_name = "youtube"
    api_version = "v3"

    try:
        youtube = googleapiclient.discovery.build(
            api_service_name, api_version, developerKey=api_key)
        snippets_req = youtube.search().list( part="snippet"
                                            , maxResults=50
                                            , publishedAfter="2019-12-31T00:00:00Z"
                                            , q=search_phrase
                                            , relevanceLanguage="en"
                                            , type="video"
                                            , videoCaption="closedCaption"
                                            )
        snippets = snippets_req.execute()
        num_items = len(snippets["items"])
        if query_file != None:
            print( str.format( "{0}\t{1}\t{2}\t{3}\t{4}"
                             , query_id
                             , num_items
                             , access(snippets, ["pageInfo", "totalResults"], "0")
                             , search_phrase
                             , time.strftime('%Y-%m-%dT%H:%M:%S')
                             )
                 , file=query_file
                 , flush=True
                 )
    except Exception as exc:
        print("\nERROR - - Exception during video search for \""+search_phrase+"\":", file=sys.stderr)
        msg = str(exc.args)
        print(msg, file=sys.stderr)
        check_quota( msg )
        return

    videos_processed = 0
    for snippet in snippets["items"]:
        if num_results >= 0 and videos_processed >= num_results:
            break
#        import pdb; pdb.set_trace()
        videos_processed = videos_processed + 1
        video_id   = access( snippet, ["id","videoId"], "" )
        channel_id = access( snippet, ["snippet","channelId"], "")
        if len(video_id) > 0 and video_id not in video_set:
            if len(channel_id)> 0 and channel_id not in channel_dict:
                try:
                    channel_req= youtube.channels().list( part="snippet"
                                                          , id=channel_id
                                                          )
                    channel_resp = channel_req.execute()
                    channel_data = channel_resp["items"][0]
                    channel_dict[channel_id] = channel_data
                except Exception as exc:
                    msg = str(exc.args)
                    print("\nERROR - "+video_id+" Exception while querying Google channels API:", file=sys.stderr)
                    print(str(exc), file=sys.stderr)
                    check_quota(msg)

            else:
                channel_data = channel_dict[channel_id]

            statistics = youtube.videos().list( part="statistics"
                                              , id=video_id
                                              )
            try:
                vid_stats_resp = statistics.execute()
                vid_stats = vid_stats_resp["items"][0]
            except Exception as exc:
                msg = str(exc.args)
                print("\nERROR - "+video_id+" Exception raised during video statistics query for "+video_id, file=sys.stderr)
                print(msg, file=sys.stderr)
                check_quota(msg)

            views = access( vid_stats, ["statistics","viewCount"], "0" )
            likes = access( vid_stats, ["statistics","likeCount"], "0" )
            if access( vid_stats, ["id"], "" ) != video_id:
                views = "0"
                likes = "0"
            if results_file != None:
                print( str.format( "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}"
                                 , query_id
                                 , video_id
                                 , "0"
                                 , access(snippet, ["snippet","publishedAt"], "1969-12-31T23:59:59Z")
                                 , access(snippet, ["snippet","title"], "noVideoTitle")
                                 , channel_id
                                 , access(snippet, ["snippet","channelTitle"], "noChannelTitle")
                                 , access(channel_data, ["snippet","country"], "" )
                                 , views
                                 , likes
                                 )
                     , file=results_file
                     , flush=True
                     )
            process_video( vid=video_id )

            commentRequest = youtube.commentThreads().list(
                part="snippet,replies",
                videoId=video_id
                )
            try:
                comments_resp = commentRequest.execute()
                comments = comments_resp["items"]

                content = "\n\n".join(map( lambda c: access(c, ['snippet','topLevelComment','snippet','textOriginal'],""), comments ))
                if len(content) > 0:
                    if zip_file == None:
                        with open(video_id+"_comments.txt","w") as fout:
                            print(content,fout)
                    else:
                        zip_file.writestr(video_id+"_comments.txt",content,compress_type=zipfile.ZIP_DEFLATED)
            except Exception as exc:
                msg = str(exc.args)
                print("\nERROR - "+video_id+" Exception raised during video comment query for "+video_id, file=sys.stderr)
                print(msg, file=sys.stderr)
                check_quota(msg)


            video_set.add( video_id )
        elif video_id in video_set:
            print("\nINFO - "+video_id+" Video already processed",file=sys.stderr)
            print( str.format( "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}"
                             , query_id
                             , video_id
                             , "1"
                             , ""
                             , ""
                             , channel_id
                             , ""
                             , ""
                             , ""
                             , ""
                             )
                 , file=results_file
                 , flush=True
                 )


def main():
    global channel_dict, num_results, query_file, query_id, results_file, video_set, zip_file
    state_file_name = ".getVideos.state"
    channel_dict = dict()

    options = argparse.ArgumentParser(description="Get YouTube video captions")
    options.add_argument( 'terms'
                        , metavar="term"
                        , type=str
                        , nargs='*'
                        , help='search term'
                        )
    options.add_argument( '-f'
                        , dest='term_file'
                        , metavar="term_file"
                        , type=str
                        , nargs=1
                        , help='file containing search terms, one per line'
                        )
    options.add_argument( '-k'
                        , dest='key_file'
                        , required=True
                        , metavar="key_file"
                        , type=str
                        , nargs=1
                        , help='name of file containinng API key to use'
                        )
    options.add_argument( '-n'
                        , dest='num_results'
                        , required=False
                        , metavar="num_results"
                        , type=int
                        , nargs=1
                        , help='Number of results to process per search term'
                        )
    options.add_argument( '-q'
                        , dest='query_file'
                        , metavar="query_file"
                        , type=str
                        , nargs=1
                        , help='file containing query overview listing term_id, total number of videos available and search term'
                        )
    
    options.add_argument( '-r'
                        , dest='results_file'
                        , metavar="results_file"
                        , type=str
                        , nargs=1
                        , help='file containing query results listing term_id, video_id, video title, channel id, and channel title in TSV format'
                        )
    options.add_argument( '-z'
                        , dest='zip_file'
                        , metavar="zip_file"
                        , type=str
                        , nargs=1
                        , help='file containing zipped video caption contents'
                        )
    args = options.parse_args()

#     key_phrase = sys.argv[2]

#    client_secrets_file = sys.argv[1]
    api_key = ""
    with open(args.key_file[0], "r") as kf:
        api_key = kf.read().strip()

    if len(api_key) == 0:
        print("\nERROR - - Failed to read api_key from "+args.key_file)
        exit

    if args.query_file != None:
        try:
            query_file = open( args.query_file[0], mode="r+" )
            pos = query_file.seek(0,os.SEEK_END)
        except FileNotFoundError as exc:
            query_file = open( args.query_file[0], mode="w" )
            print( str.format( "{0}\t{1}\t{2}\t{3}\t{4}"
                             , "queryId"
                             , "numResults"
                             , "totResults"
                             , "searchPhrase"
                             , "dateTime"
                             )
                 , file=query_file
                 , flush=True
                 )

    if args.results_file != None:
        try:
            results_file = open( args.results_file[0], mode="r+" )
            pos = results_file.seek(0,os.SEEK_END)
        except:
            results_file = open( args.results_file[0], mode="w" )
            print( str.format( "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}"
                             , "queryId"
                             , "videoId"
                             , "dup"
                             , "publishedAt"
                             , "title"
                             , "channelId"
                             , "channelTitle"
                             , "country"
                             , "views"
                             , "likes"
                             )
                 , file=results_file
                 , flush=True
                 )

    if args.zip_file != None:
        try:
            zip_file = zipfile.ZipFile( args.zip_file[0], mode="x" )
        except FileExistsError as exc:
            zip_file = zipfile.ZipFile( args.zip_file[0], mode="a" )

    try:
        state_file = open( state_file_name, mode="r+" )
        txt = state_file.readline().strip()
        query_id = int(txt)
        video_set = set() 
        for line in state_file:
            video_id = line.strip()
            if video_id not in video_set:
                video_set.add(video_id)
        state_file.close()
    except Exception as exc:
        query_id = -1
        video_set = set()

    if args.num_results != None and len(args.num_results) > 0 :
        num_results = args.num_results[0]

    if args.terms != None and len(args.terms) > 0:
        for search_term in args.terms:
            try:
                video_search( api_key=api_key, search_phrase=search_term )
            except Exception as exc:
                msg = str(exc.args)
                print("\nERROR - - Exception occurred durng video_search:", file=sys.stderr)
                print( str(exc.args ), file=sys.stderr)
                check_quota(msg)

    elif args.term_file[0] != None:
        with open(args.term_file[0])  as tf:
            for line in  tf:
                search_term = line.strip()
                try:
                    video_search( api_key=api_key, search_phrase=search_term )
                except Exception as exc:
                    msg = str(exc.args)
                    print("\nERROR - - Exception occurred during video search:", file=sys.stderr)
                    print(msg, file=sys.stderr)
                    check_quota(msg)

    # save the state of processing
    state_file = open( state_file_name, mode="w" )
    print(str(query_id),file=state_file)
    for video_id in video_set:
        print(video_id,file=state_file)
    state_file.close()


    if args.query_file != None:
        query_file.close()

    if args.results_file != None:
        results_file.close()

    if args.zip_file != None:
        zip_file.close()


if __name__ == "__main__":
    main()
