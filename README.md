# Retrieve YouTube transcripts based on search terms

This repository contains a Python program ```getVideos.py``` that downloads
transcripts from videos selected by search terms.  It uses the Google 
YouTube Data API v3, to search for videos matching specific terms and retrieve
metadata about selected videos and their creators. It then downloads 
English language transcript text using the Python youtube_transcript_api 
package version 0.3.1.  The program is executed using the Anaconda Python 3.6.8 
framework.  

Output from the program consists a tab separated values (TSV) file containing a list of query
terms used to select videos.  Each line has the number of videos selected, the
total number of videos which satisfied the search terms and the search term.
It also generates a TSV file containing one line for each video processed containing
a query number, the video identifier, title and date of publication, the creator's 
identifier and name, as well as a duplicate indicator and the nation code where the 
creator resides, if provided. Finally, the program produces a zip file containing all of 
the transcript text with each video represented by a single file.

The open source code for this is provided in a public Gitlab repository, [YouTube Videos](https://gitlab.com/roggsky/YouTubeVideos)